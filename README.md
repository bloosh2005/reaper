#  reaper

> reaper RESTful webservice platform for [katana wechat conference application](https://bitbucket.org/nonumber1989/katana)


## Description
> reaper is a RESTFul webservice platform for an application katana  (used for wechat conference)

> This project use Restify framework + Mongoose + MongoDB + other nodejs modules 


## Build &  Development

make sure you have install nodejs environment and MongodDB
#### make sure the MongodDB started 


    git clone https://bitbuckt.org/nonumber1989/reaper
    npm install 
    node server.js
    
User can use Rest Client or other rest utils to preview 

## License

MIT © [steven xu](nonumber1989)