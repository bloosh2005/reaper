var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var WeChatUserSchema = new Schema({
    subscribe: Number,
    openid: String,
    nickname: String,
    sex: Number,
    language:String,
    city:String,
    province:String,
    country:String,
    headimgurl:String,
    subscribe_time:Date,
    unionid:String,
    remark:String,
    groupid:Number
});
mongoose.model('WeChatUser', WeChatUserSchema);
