var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Speaker = mongoose.model("Speaker");

var requestUtils = require('../services/requestUtils');

//view speakers by pagenation
router.get('/',function (req, res, next) {
	var pagenation = requestUtils.getPagenation(req);
    Speaker.find({},{},pagenation, function (err, speakers) {
        if (err) {
            res.status(500);
            res.json({
                type: false,
                data: "Error occured: " + err
            });
        } else {
            if (speakers) {
                res.json({
                    type: true,
                    data: speakers
                });
            } else {
                res.json({
                    type: false,
                    data: "no speakers found"
                });
            }
        }
    })
});

//create a new speaker
router.post('/', function (req, res, next) {
    var speakerModel = new Speaker(req.body);
    speakerModel.save(function (err, speaker) {
        if (err) {
            res.status(500);
            res.json({
                type: false,
                data: "Error occured: " + err
            })
        } else {
            res.json({
                type: true,
                data: speaker
            })
        }
    })
});

module.exports = router;
